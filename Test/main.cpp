//
//  main.cpp
//  Test
//
//  Created by Joe Lee on 2014-8-29.
//  Copyright (c) 2014年 Joe. All rights reserved.
//

/**
 *  Search maximum and minimum in tree
 */

#include <iostream>
#include <vector>

typedef struct TreeNode {
  struct TreeNode *leftChild;
  struct TreeNode *rightChild;
  int value;

  TreeNode(int value, TreeNode *leftChild, TreeNode *rightChild)
  : value(value), leftChild(leftChild), rightChild(rightChild){}
} TreeNode;

void maximumAndMinimumInTree(TreeNode *treeRoot, int &min, int &max)
{
  if (NULL == treeRoot) { return; }

  if (treeRoot->value < min) {
    min = treeRoot->value;
  } else if (treeRoot->value > max) {
    max = treeRoot->value;
  }

  maximumAndMinimumInTree(treeRoot->leftChild, min, max);
  maximumAndMinimumInTree(treeRoot->rightChild, min, max);
}

void maximumAndMinimumInTreeWithoutRecursion(TreeNode *treeRoot, int &min, int &max)
{
  std::vector<TreeNode> stack;

  TreeNode *node = treeRoot;
  while (!stack.empty() || NULL != node) {
    if (NULL == node) {
      node = &stack.back();
      stack.pop_back();
    }
    if (NULL == node) {
      break;
    }

    if (node->value < min) {
      min = node->value;
    } else if (node->value > max) {
      max = node->value;
    }
    if (NULL != node->rightChild) {
      stack.push_back(*node->rightChild);
    }
    node = node->leftChild;
  }
}

unsigned maxDiffAbsoluteValue(TreeNode *treeRoot, bool withRecursion)
{
  if (NULL == treeRoot) { return 0; }

  int min = treeRoot->value, max = treeRoot->value;
  if (withRecursion) {
    maximumAndMinimumInTree(treeRoot, min, max);
  } else {
    maximumAndMinimumInTreeWithoutRecursion(treeRoot, min, max);
  }

  return (max > min) ? (max - min) : 0;
}

int main(int argc, const char * argv[])
{
  // make a test case
  TreeNode *left_left = new TreeNode(6, NULL, NULL);
  TreeNode *left_right = new TreeNode(-12, NULL, NULL);
  TreeNode *left = new TreeNode(2, left_left, left_right);

  TreeNode *right_left = new TreeNode(4, NULL, NULL);
  TreeNode *right_right = new TreeNode(8, NULL, NULL);
  TreeNode *right = new TreeNode(15, right_left, right_right);

  TreeNode *root = new TreeNode(10, left, right);

  unsigned diffValue = maxDiffAbsoluteValue(root, true);
  std::cout << "\ndiffValue (with recursion) = " << diffValue << "\n" << std::endl;
  diffValue = maxDiffAbsoluteValue(root, false);
  std::cout << "\ndiffValue (without recursion) = " << diffValue << "\n" << std::endl;

  return 0;
}
